return {
  ["workshop-1207269058"]={ configuration_options={  }, enabled=true },
  ["workshop-1242093526"]={ configuration_options={ glowcap=1, mushlight=1, winter_tree=0 }, enabled=true },
  ["workshop-1430042811"]={ configuration_options={  }, enabled=true },
  ["workshop-1595631294"]={
    configuration_options={
      BundleItems=false,
      Digornot=true,
      DragonflyChest=true,
      Icebox=false,
      SaltBox=false 
    },
    enabled=true 
  },
  ["workshop-1803285852"]={
    configuration_options={
      [""]=0,
      AutoPickupAsh=false,
      AutoPickupEnabled=false,
      AutoPickupPoop=false,
      AutoPickupRange=10,
      AutoPickupSeeds=false,
      AutoStackAsh=false,
      AutoStackEnabled=true,
      AutoStackMakeNewStackMainStack=true,
      AutoStackManuallyDroppedItems=false,
      AutoStackPoop=false,
      AutoStackRange=10,
      AutoStackSeeds=false,
      AutoStackTwiggyTreeTwigs=false,
      ManualDropStackRange=5,
      ManualStackAsh=true,
      ManualStackMakeNewStackMainStack=false,
      ManualStackPoop=true,
      ManualStackSeeds=true,
      PlayerMustHaveOneOfItemToAutoPickup=false,
      SmokePuffOnStacking=true,
      StackDuringPopulation=false 
    },
    enabled=true 
  },
  ["workshop-1852559465"]={ configuration_options={  }, enabled=true },
  ["workshop-1894295075"]={ configuration_options={  }, enabled=true },
  ["workshop-1932983865"]={ configuration_options={ recipe_complexity="normal" }, enabled=true },
  ["workshop-2012031749"]={
    configuration_options={
      _DEVG_override=1,
      _HOUND_override=1,
      _LEIF_override=1,
      _OASISEXTRA_override=0,
      _OASIS_override=0,
      _PIGS_override=0,
      _POI_override=3,
      _PR_override=3,
      _REEDS_override=1,
      _SPIDER_override=0,
      _TALLB_override=1,
      _TR_override=1 
    },
    enabled=true 
  },
  ["workshop-2032577827"]={
    configuration_options={ detect_witherable=1, emergency_time=5, fuel_time=1, turn_on_time=2 },
    enabled=true 
  },
  ["workshop-2032685784"]={ configuration_options={  }, enabled=true },
  ["workshop-347079953"]={
    configuration_options={ DFV_Language="EN", DFV_MinimalMode="default" },
    enabled=true 
  },
  ["workshop-356930882"]={ configuration_options={ uses=10000000, uses2=10000000 }, enabled=true },
  ["workshop-362175979"]={ configuration_options={ ["Draw over FoW"]="disabled" }, enabled=true },
  ["workshop-375859599"]={
    configuration_options={
      divider=5,
      random_health_value=0,
      random_range=0,
      send_unknwon_prefabs=false,
      show_type=0,
      unknwon_prefabs=1,
      use_blacklist=true 
    },
    enabled=true 
  },
  ["workshop-378160973"]={
    configuration_options={
      ENABLEPINGS=true,
      FIREOPTIONS=2,
      OVERRIDEMODE=false,
      SHAREMINIMAPPROGRESS=true,
      SHOWFIREICONS=true,
      SHOWPLAYERICONS=true,
      SHOWPLAYERSOPTIONS=2 
    },
    enabled=true 
  },
  ["workshop-380423963"]={
    configuration_options={
      [""]=0,
      boulder_blue=0.2,
      boulder_purple=0.05,
      change_cave_loot=false,
      common_loot_charcoal=0,
      common_loot_flint=0.35,
      common_loot_rocks=0.35,
      cutlichen=0,
      durian=0,
      flintless_blue=0.5,
      flintless_purple=0.1,
      flintless_red=0.2,
      foliage=0,
      gears=0,
      goldvein_purple=0.05,
      goldvein_red=0.1,
      guano=0,
      ice=0,
      lightbulb=0,
      moon_green=0.02,
      moon_orange=0.02,
      moon_yellow=0.02,
      pinecone=0,
      rare_loot_bluegem=0.015,
      rare_loot_marble=0.015,
      rare_loot_redgem=0.015,
      rottenegg=0,
      seeds=0,
      spoiled_food=0,
      stalagmite_green=0.02,
      stalagmite_orange=0.02,
      stalagmite_yellow=0.02,
      uncommon_loot_goldnugget=0.05,
      uncommon_loot_mole=0.05,
      uncommon_loot_nitre=0.05,
      uncommon_loot_rabbit=0.05 
    },
    enabled=true 
  },
  ["workshop-382177939"]={
    configuration_options={ [""]=true, chillit="yep", eightxten="5x16", workit="yep" },
    enabled=true 
  },
  ["workshop-398858801"]={
    configuration_options={
      afk_enabled=true,
      afk_host_immunity=true,
      afk_hunger_decrease=true,
      afk_max_action=1,
      afk_max_time=0,
      afk_stop_beaver=true,
      afk_stop_death=true,
      afk_stop_sanity=true,
      afk_stop_wet=true,
      afk_temp=true,
      afk_time=120 
    },
    enabled=true 
  },
  ["workshop-466732225"]={ configuration_options={  }, enabled=true },
  ["workshop-478005098"]={ configuration_options={  }, enabled=true },
  ["workshop-503795626"]={
    configuration_options={ Invincible_Wall=true, More_Invincible_Wall=false },
    enabled=true 
  },
  ["workshop-551324730"]={ configuration_options={  }, enabled=true },
  ["workshop-666155465"]={
    configuration_options={
      chestB=-1,
      chestG=-1,
      chestR=-1,
      food_estimation=-1,
      food_order=0,
      food_style=0,
      lang="auto",
      show_food_units=-1,
      show_uses=-1 
    },
    enabled=true 
  },
  ["workshop-782961570"]={ configuration_options={ layout=0, position=0 }, enabled=true },
  ["workshop-785009843"]={ configuration_options={  }, enabled=true },
  ["workshop-818739975"]={ configuration_options={ DFV_Language="EN" }, enabled=true },
  ["workshop-856487758"]={ configuration_options={  }, enabled=true },
  ["workshop-875994715"]={ configuration_options={ charcoal=true }, enabled=true } 
}